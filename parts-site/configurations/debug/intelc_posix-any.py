######################################
### Intel posix compiler configurations pat_debug
######################################

from parts.config import *

def map_default_version(env):
    return env['INTELC_VERSION']
    

config=configuration(map_default_version)


config.VersionRange("2020.*",
                    append=ConfigValues(
                        #CCFLAGS=['-O0','-g', '-Werror', '-Wextra'],
                        CXXFLAGS=['-std=c++17'],
                        CPPDEFINES=['DEBUG'],
                        LINKFLAGS=['-static-intel']
                        #LINKFLAGS=['-static-intel','-Wl,--as-needed']
                        )
                    )
                    
