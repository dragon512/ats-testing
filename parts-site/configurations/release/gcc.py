######################################
### gcc compiler configurations debug
######################################

from parts.config import *

def map_default_version(env):
    return env['GCC_VERSION']
    
config=configuration(map_default_version)

config.VersionRange("8.*,9.*",
                    append=ConfigValues(
                        CCFLAGS=['-O2','-g', '-Werror', '-Wextra'],
                        #CFLAGS=['-Wpedantic'],
                        CXXFLAGS=['-std=c++17'],
                        CPPDEFINES=['NDEBUG'],
                        #LINKFLAGS=['-Wl,--as-needed']
                        )
                    )

