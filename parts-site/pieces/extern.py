import sys
from pathlib import Path
from typing import Tuple, Optional
import yaml

import distro
import parts.api as api
import parts.glb as glb
from parts.version import version
import parts.vcs.git as scm
from parts.parts import Part_factory as Part


def get_distro() -> Tuple[str, str]:
    if sys.platform =='win32':
        return ("--unknown--", "0.0.0")
    elif sys.platform =='darwin':
        return ("--unknown--", "0.0.0")
    else:
        return distro.linux_distribution()


def GetDistro() -> str:
    if sys.platform =='win32':
        return "--unknown--"
    elif sys.platform =='darwin':
        return "--unknown--"
    else:
        info = distro.linux_distribution()
        name = info[0]
        if name == "Red Hat":
            name == "RHEL"
        return name


def isDistro(name, version=None) -> bool:
    if sys.platform =='win32':
        return False
    elif sys.platform =='darwin':
        return False
    else:
        info = distro.linux_distribution()
        if version:
            return info[0] == name and info[1] == version
        return info[0] == name



def DistroVersion() -> version:
    return version(get_distro()[1])


def LoadComponents(group=None) -> None:
    '''
    The logic for loading a yaml file is to look for file in the format of:
        <name>.parts.yaml

        Where "name" is would be defined by the user or in a predefined value
        Predefined values would be in the form of

        <distro>.<distro version>
        <distro>
        <OS> where is OS is builtin platform values such as win32 darwin bsd posix

        source ??
        system ??

    '''

    # target_platform =
    if group:
        match_list = [
            f"{group}.{GetDistro()}.{DistroVersion()}.parts.yaml",
            f"{group}.{GetDistro()}.parts.yaml",
            # f"{group}.{target_platform}.parts.yaml",
            # f"{group}.{target_os}.parts.yaml",
            f"{group}.{GetDistro()}.parts.yaml",
            f"{group}.Sconscript.parts.yaml",
            f"{group}.parts.yaml",
        ]
    else:
        match_list = [
            f"{GetDistro()}.{DistroVersion()}.parts.yaml",
            f"{GetDistro()}.parts.yaml",
            # f"{target_platform}.parts.yaml",
            # f"{target_os}.parts.yaml",
            f"{GetDistro()}.parts.yaml",
            f"Sconscript.parts.yaml",
        ]

    #build_from = GetOption("build_from")
    #install_dep = GetOption("install_depends")

    # try to figure out what set use
    yaml_file:Optional[Path] = None

    for entrystr in match_list:
        entry = Path(entrystr)
        if entry.exists():
            api.output.print_msg(f"Found {entry}!")
            yaml_file = entry
            break
        else:
            api.output.verbose_msg(["component.load"], f"{entry} not found...")
    else:
        api.output.error_msg(f"Valid component yaml file not found!", show_stack=False)

    part_data = yaml.load(yaml_file.read_text(), Loader=yaml.FullLoader)
    process_parts_yaml(part_data)

def buildgit(gitinfo:dict) -> scm.git:
    gitinfo['repository'] = gitinfo.get('repository',gitinfo.get('repo'))
    if gitinfo['repository'] is None:
        api.output.error_msg("repository must be defined for ScmGit object")
    if "repo" in gitinfo:
        del gitinfo['repo']
    if "type" in gitinfo:
        del gitinfo['type']
    api.output.verbose_msg(["scm.component.load","component.load"], "Defining ScmGit with args", gitinfo)
    scmobj = scm.git(**gitinfo)
    return scmobj

def get_item(key,data):
    ret = data.get(key)
    if ret:
        del data[key]
    return ret

def process_parts_yaml(yaml_data):
    known_externs = {}
    parts_data = yaml_data['parts']
    config_yamls = set()
    for alias, item in parts_data.items():

        ##########################
        # get common information

        # get part file name
        filename = Path(get_item('file',item))

        # define the configure.yaml (wip)
        config_yaml = f"{filename.stem}.configure.yaml"

        # get any mode values
        mode = get_item('mode',item)

        # create sdk
        create_sdk = get_item('create_sdk',item)

        # create package_group
        package_group = get_item('package_group',item)

        # get any value we append
        append = get_item('append',item)

        # get any value we prepend
        prepend = get_item('prepend',item)


        ################################
        # get any scm info
        scmtmp = get_item('scm',item)
        scmobj = None
        if scmtmp:
            scm_type = scmtmp.get("type")
            if scm_type is None:
                scmobj = buildgit(scmtmp)
            elif scm_type == "git":
                scmobj = buildgit(scmtmp)
            else:
                api.output.error_msg("scm type not Implemented:", scm_type)


        ################################
        # get extern information
        extern = get_item('extern',item)
        if extern:
            # temp ... till better setup !!!!!!!!!!
            extern['repository'] = extern.get('repository',extern.get('repo','dragon512/extern-parts'))
            extern['server'] = extern.get('server','bitbucket.org')

            scm_type = extern.get("type")
            if scm_type is None:
                externobj = buildgit(extern)
            elif scm_type == "git":
                externobj = buildgit(extern)
            else:
                api.output.error_msg("scm type not Implemented:", scm_type)

            # define repo alias value for extern repo
            extern_alias = "{}-{}-{}".format(
                extern.get("server", 'default'),
                extern.get("repo", 'default'),
                extern.get("branch", extern.get("tag", extern.get("revision", 'default')))
                )
            # is this a known extern repo
            if extern_alias not in known_externs:
                # if not we make it a part to be checked out
                pobj = Part(extern_alias, "__extern__.part", vcs_type=externobj,
                            CHECK_OUT_ROOT='$EXTERN_PARTS_ROOT', EXTERN_ROOT="#_extern")[0]
                # store the path to the repo
                # this path is absolute
                known_externs[extern_alias] = Path(pobj.SourcePath)


            # update part file to point to the external location
            # get the base path we have stored... this is absolute path
            base_path = known_externs[extern_alias]
            config_yamls.add(base_path / config_yaml)
            filename = f'{base_path / filename}'

        else:
            config_yamls.add(config_yaml)

        # allow us to do configure logic after all the parts are loaded
        # glb.engine.add_preprocess_logic_queue(process_configure(config_yamls))  ## ugg .. need a preprocess queue

        # extra stuff to pass in
        extra = item
        api.output.verbose_msg(["component.load"],f"Defining Part: Part({alias}, {filename}, vcs_type={scmobj}, **{extra})")
        Part(alias=alias, parts_file=filename, vcs_type=scmobj, **extra)


api.register.add_variable('EXTERN_PARTS_ROOT', '#_externs',
                          'Defines root location for external component repo to be checked out to')

api.register.add_global_object('LoadComponents', LoadComponents)
